alembic==1.5.8
PyJWT==2.0.1
FastAPI-SQLAlchemy==0.2.1
passlib==1.7.4
environs==9.5.0
email-validator==1.1.2
python-dotenv==0.15.0
