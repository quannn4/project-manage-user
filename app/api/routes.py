from fastapi import APIRouter

from app.api.api_login import router as login_router
from app.api.api_register import router as register_router
from app.api.coffee import router as coffee_router
from app.api.users import router as users_router

router = APIRouter()

router.include_router(router=users_router, prefix="/users", tags=["users"])
router.include_router(router=coffee_router, prefix="/coffee", tags=["coffee"])
router.include_router(router=login_router, prefix="/login", tags=["login"])
router.include_router(router=register_router, prefix="/register", tags=["register"])
