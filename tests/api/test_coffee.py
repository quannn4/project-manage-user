import pytest
from fastapi.testclient import TestClient

from app.main import app_factory

client = TestClient(app_factory)


@pytest.mark.asyncio
async def test_create_coffee():
    sample_coffee = {
        "blend_name": "Sample Blend",
        "origin": "Sample Origin",
        "variety": "Sample Variety",
        "notes": ["Sample Note 1", "Sample Note 2"],
        "intensifier": "Sample Intensifier",
    }

    response = await client.post("/", json=sample_coffee)

    assert response.status_code == 200

    response_data = response.json()
    assert "status" in response_data
    assert "message" in response_data
    assert "data" in response_data

    assert response_data["status"] == "success"
    assert response_data["message"] == "Operation successful"

    coffee_data = response_data["data"]
    assert "id" in coffee_data
    assert "blend_name" in coffee_data
    assert "origin" in coffee_data
    assert "variety" in coffee_data
    assert "notes" in coffee_data
    assert "intensifier" in coffee_data
    assert "users" in coffee_data
    assert coffee_data["blend_name"] == sample_coffee["blend_name"]
    assert coffee_data["origin"] == sample_coffee["origin"]
    assert coffee_data["variety"] == sample_coffee["variety"]
    assert coffee_data["notes"] == sample_coffee["notes"]
    assert coffee_data["intensifier"] == sample_coffee["intensifier"]
    assert coffee_data["users"] == []


@pytest.mark.asyncio
async def test_search_coffee():
    response = await client.get("/")

    assert response.status_code == 200

    response_data = response.json()
    assert "status" in response_data
    assert "message" in response_data
    assert "data" in response_data

    assert response_data["status"] == "success"
    assert response_data["message"] == "Operation successful"

    coffee_list = response_data["data"]
    assert isinstance(coffee_list, list)
    for coffee_data in coffee_list:
        assert "id" in coffee_data
        assert "blend_name" in coffee_data
        assert "origin" in coffee_data
        assert "variety" in coffee_data
        assert "notes" in coffee_data
        assert "intensifier" in coffee_data
        assert "users" in coffee_data

        assert isinstance(coffee_data["users"], list)
